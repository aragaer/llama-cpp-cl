#pragma once

#include <llama.h>

#ifdef __cplusplus
extern "C" {
#endif

struct llama_grammar *llama_grammar_parse(char *src);

#ifdef __cplusplus
}
#endif
