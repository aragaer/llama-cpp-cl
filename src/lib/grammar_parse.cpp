#include <llama.h>
#include <common/grammar-parser.h>

#include "grammar.h"

struct llama_grammar *llama_grammar_parse(char *src) {
  struct grammar_parser::parse_state result = grammar_parser::parse(src);

  if (result.rules.empty())
    return NULL;

  return llama_grammar_init(result.c_rules().data(),
                            result.c_rules().size(),
                            result.symbol_ids.at("root"));
}
