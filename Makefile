# Configuration -- should be edited

LIBRESECT = $(HOME)/Projects/libresect/build/resect/libresect.so
CCL = $(HOME)/Projects/ccl/lx86cl64

LLAMA_CUBLAS = 1

# Configuration end

OUT_DIR = lib
BUILD_DIR = build

BINDINGS_ASD = claw-llama-cpp-bindings.asd
BINDINGS_LISP = bindings/x86_64-pc-linux-gnu.lisp

ADAPTER_SRC = src/lib/adapter.x86_64-pc-linux-gnu.c
ADAPTER_LIB = $(OUT_DIR)/libllama-adapter.so

LIB_DIR = src/lib/llama.cpp
LLAMA_LIB = $(OUT_DIR)/libllama.so
LLAMA_H = $(LIB_DIR)/llama.h

GRAMMAR_ADAPTER_OBJ = $(BUILD_DIR)/grammar_parse.o $(BUILD_DIR)/grammar-parser.o

all: $(BINDINGS_ASD) $(BINDINGS_LISP) $(LLAMA_LIB) $(ADAPTER_LIB)

$(ADAPTER_LIB) $(LLAMA_LIB): | $(OUT_DIR)

$(OUT_DIR) $(BUILD_DIR):
	mkdir -p $@

$(ADAPTER_LIB): $(ADAPTER_SRC) $(GRAMMAR_ADAPTER_OBJ) | $(LLAMA_LIB)
	@echo will now compile
	$(CC) -o $@ -fPIC $^ -I$(LIB_DIR) -shared

$(LLAMA_LIB): src/lib/llama.cpp
	$(MAKE) -C $(LIB_DIR) libllama.so
	cp $(LIB_DIR)/libllama.so $@

$(ADAPTER_SRC) $(BINDINGS_LISP): $(BINDINGS_ASD)

$(BINDINGS_ASD): $(LLAMA_H)
	@echo "Generating bindings"
	LIBRESECT=$(LIBRESECT) $(CCL) -Q -n -b -l generate.lisp -e '(quit)'

$(BUILD_DIR)/grammar_parse.o : src/lib/grammar_parse.cpp
$(BUILD_DIR)/grammar-parser.o : src/lib/llama.cpp/common/grammar-parser.cpp
$(BUILD_DIR)/%.o : | $(BUILD_DIR)
	$(CXX) -c -o $@ $^ -I$(LIB_DIR) -fPIC

clean:
	rm -rf $(OUT_DIR) $(BINDINGS_ASD) $(BINDINGS_LISP) $(BUILD_DIR)
	$(MAKE) -C $(LIB_DIR) clean

.PHONY: clean all
